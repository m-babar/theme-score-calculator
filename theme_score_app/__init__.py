from flask import Flask
from flask_pymongo import PyMongo

from theme_score_app.config import Config

mongo = PyMongo()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)

    mongo.init_app(app)

    from theme_score_app.main.routes import main
    from theme_score_app.errors.handlers import errors

    app.register_blueprint(main)
    app.register_blueprint(errors)

    return app
