import os


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY', "f61d941196bb0df7acc2537843547d3045a3e64683dc9a45a8ea98440f71d108")
    MONGO_URI = os.environ.get('MONGO_URI', "mongodb://localhost:27017/flask-video")
