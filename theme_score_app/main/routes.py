# from datetime import datetime

from flask import Blueprint, flash, redirect, render_template, url_for
from bson import ObjectId

from theme_score_app import mongo

from theme_score_app.main.forms import VideoForm

main = Blueprint("main", __name__)
video_db = mongo.db.videos


@main.route("/", methods=["GET", "POST"])
def home():
    form = VideoForm()
    if form.validate_on_submit():
        video_db.insert(
            {
                "title": form.title.data,
                "theme": form.theme.data,
                "thumbs_up": 0,
                "thumbs_down": 0,
            }
        )
        flash(f"Video created.!!!!!", "success")
        return redirect(url_for("main.home", videos=get_videos()))
    return render_template(
        "home.html", form=form, legend="Create Video", videos=get_videos()
    )


@main.route("/themes", methods=["GET"])
def theme_list():
    themes = video_db.aggregate(
        [
            {
                "$group": {
                    "_id": "$theme",
                    "score": {"$avg": {"$subtract": ["$thumbs_up", "$thumbs_down"]}},
                }
            },
            {"$sort": {"score": -1}},
        ]
    )
    return render_template("theme_list.html", themes=themes)


@main.route("/video/<video_id>/thumb_up")
def thumbs_up(video_id):
    try:
        video = video_db.find({"_id": ObjectId(video_id)})
        thumbs_up = video[0]["thumbs_up"] + 1
        video_db.update({"_id": ObjectId(video_id)}, {"$set": {"thumbs_up": thumbs_up}})
    except Exception:
        flash(f"Video Not Found.!!!!!", "warning")

    return redirect(url_for("main.home", videos=get_videos()))


@main.route("/video/<video_id>/thumb_down")
def thumbs_down(video_id):
    try:
        video = video_db.find({"_id": ObjectId(video_id)})
        thumbs_down = video[0]["thumbs_down"] + 1
        video_db.update(
            {"_id": ObjectId(video_id)}, {"$set": {"thumbs_down": thumbs_down}}
        )
    except Exception:
        flash(f"Video Not Found.!!!!!", "warning")

    return redirect(url_for("main.home", videos=get_videos()))


def get_videos():
    return video_db.find()
