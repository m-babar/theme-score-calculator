from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length


class VideoForm(FlaskForm):
    title = StringField('Video Title', validators=[DataRequired(), Length(min=1, max=164)])
    theme = StringField('Video Theme', validators=[DataRequired()])
    submit = SubmitField('Submit')
