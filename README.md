# Theme Score Calculator

# Project Title

Counts the score of video themes based on likes and dislikes. Created using Flask and MongoDB

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Python 3.7
Mongodb >=3.4

### Installing

Create a virtual environment and activate

```
virtualenv env --python=python3.7
source env/bin/activate
```

Install the requirements

```
pip install -r requirements.txt
```
Set Mongo uri

```
export MONGO_URI='mongodb://localhost:27017/theme-score-app'
```

Run the following command to get your project up and running
```
python run.py
```
